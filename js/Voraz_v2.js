const hldr_string = ["Arad", "Lugoj", "Rimmicu Vilcea",
                    "Craiova", "Mehadia", "Sibiu",
                    "Dobreta", "Oradea", "Timisoara",
                    "Fagaras", "Pitesi", "Zerind",
                    "Bucarest"];

const hldr_int = [  366, 244, 193,
                    160, 241, 253,
                    242, 380, 329,
                    176, 100, 374,
                    0];

const caminos = [[5, 8, 11, -1], [8, 4, -1, -1], [5, 10, 3, -1],
                [6, 2, 10, -1], [1, 6, -1, -1], [0, 7, 9, 2],
                [4, 3, -1, -1], [11, 5, -1, -1], [0, 1, -1, -1],
                [5, 12, -1, -1], [2, 3, 12, -1], [0, 7, -1, -1],
                [0, 0, 0, 0]];
                
    document.write('<div id="inicio"></div>');
    let ini = document.getElementById("inicio");

    const selector = document.getElementById("selector");
        for (let liz=0; liz < hldr_string.length-1; liz++) {
            selector.innerHTML += '<option value="#">'+hldr_string[liz]+'</option>';
        }
    
    
    
    ini.innerHTML += '<button id="empieza" onclick="voraz()">Empezar</button>'+
                    '<button onclick="limpiar()">Limpiar</button>'+
                    '<button id="volver"><a href="index.html">Regresar</a></button>';
    
    const empieza = document.getElementById("empieza");

    document.write('<div id="rutas"></div>')
    let ruta = document.getElementById("rutas");

    let start;
    //captura lo seleccionado de la lista
    function captura() {
        start = selector.options[selector.selectedIndex].text;
        empieza.style.display = 'inline-block';
    }


let partida;
let actual;
let pos;
let posi;
let colum;
let menor;


function voraz() {
    
    posi = buscarCiudad(start);

for (let s = 0; s < hldr_string.length; s++) {
    s = posi;
    if (posi == s) {
        ruta.innerHTML += '<br>Ciudad actual:<b> ' + hldr_string[s] + '</b><br>';
        for (let b = 0; b < 4; b++) {
            ruta.innerHTML += '<b>Ruta '+ (b + 1) + '</b>: ' + hldr_string[caminos[s][b]] + ' - Hldr: ' + hldr_int[caminos[s][b]] + '<br>';
            if (b == 3) {

            } else {
                if (caminos[s][b + 1] == -1) {
                    break;
                } else {
                    if (b == 0) {
                        if (hldr_int[caminos[s][b]] > hldr_int[caminos[s][b + 1]]) {
                            menor = hldr_int[caminos[s][b + 1]];
                            actual = hldr_string[caminos[s][b + 1]];
                            posi = caminos[s][b + 1];
                        } else {
                            menor = hldr_int[caminos[s][b]];
                            actual = hldr_string[caminos[s][b]];
                            posi = caminos[s][b];
                        }

                    } else {
                        if (menor > hldr_int[caminos[s][b + 1]]) {
                            menor = hldr_int[caminos[s][b + 1]];
                            actual = hldr_string[caminos[s][b + 1]];
                            posi = caminos[s][b + 1];
                        } else {
                            
                        }
                    }
                }

            }
        }
        ruta.innerHTML += 'Ciudad con hldr más corto: <b>' + actual + ' - Hldr: ' + menor + '</b><br><br>';
    }
    if (actual=="Bucarest") {
        ruta.innerHTML += 'Llegamos al destino deseado: <b>' + actual + '</b>';
        break;
    }
}
}

//función para buscar la ciudad inicial
function buscarCiudad(inicio) {
    let pos = hldr_string.indexOf(inicio);
    return pos;
}
//limpia los recorridos
function limpiar() {
    ruta.innerHTML = '';
}