const hldr_string = ["Arad", "Lugoj", "Rimmicu Vilcea",
    "Craiova", "Mehadia", "Sibiu",
    "Dobreta", "Oradea", "Timisoara",
    "Fagaras", "Pitesi", "Zerind",
    "Bucarest"];

const hldr_int = [
    366, 244, 193,
    160, 241, 253,
    242, 380, 329,
    176, 100, 374,
    0];

const h_int = [[140, 118, 75, -1], [111, 70, -1, -1], [80, 97, 146, -1],
[120, 146, 138, -1], [70, 75, -1, -1], [140, 151, 99, 80],
[75, 120, -1, -1], [71, 151, -1, -1], [118, 111, -1, -1],
[99, 211, -1, -1], [97, 138, 101, -1], [75, 71, -1, -1]];

const caminos = [[5, 8, 11, -1], [8, 4, -1, -1], [5, 10, 3, -1],
[6, 2, 10, -1], [1, 6, -1, -1], [0, 7, 9, 2],
[4, 3, -1, -1], [11, 5, -1, -1], [0, 1, -1, -1],
[5, 12, -1, -1], [2, 3, 12, -1], [0, 7, -1, -1]];

let partida;
let actual;
let pos;
let posi;
let colum;
let menor;
let verif;
let suma;
let acum = 0;


//do {
    console.log("Rumbo a Bucarest");
    console.log("Escriba correctamente la ciudad que será el punto de partida: ");
    //partida = scanner.nextLine();
    posi = buscarCiudad();
    console.log(hldr_string[posi]);
//} while (!verif);



for (let s = 0; s < 12; s++) {
    s = posi;

    if (posi == s) {
        
        console.log("Ciudad actual: " + hldr_string[s]);
        for (let b = 0; b < 4; b++) {
            suma = hldr_int[caminos[s][b]] + h_int[s][b] + acum;
            console.log("Ruta " + (b + 1) + ": " + hldr_string[caminos[s][b]] + " - f(" + (b + 1) + "): " + hldr_int[caminos[s][b]] +
                " - g(" + (b + 1) + "): " + h_int[s][b] + " + acumulado: " + acum + " --> " + suma);
            if (b == 3) {

            } else {
                if (caminos[s][b + 1] == -1) {
                    break;
                } else {
                    if (b == 0) {
                        if ((hldr_int[caminos[s][b]] + h_int[s][b] + acum) > (hldr_int[caminos[s][b + 1]] + h_int[s][b + 1]) + acum) {
                            menor = (hldr_int[caminos[s][b + 1]] + h_int[s][b + 1] + acum);
                            actual = hldr_string[caminos[s][b + 1]];
                            posi = caminos[s][b + 1];
                            colum = b + 1;
                            suma = hldr_int[caminos[s][b + 1]] + h_int[s][b + 1] + acum;
                        } else {
                            menor = (hldr_int[caminos[s][b]] + h_int[s][b] + acum);
                            actual = hldr_string[caminos[s][b]];
                            posi = caminos[s][b];
                            colum = b;
                            suma = hldr_int[caminos[s][b]] + h_int[s][b] + acum;
                        }

                    } else {
                        if (menor > (hldr_int[caminos[s][b + 1]] + h_int[s][b + 1]) + acum) {
                            menor = acum + hldr_int[caminos[s][b + 1]] + acum;
                            actual = hldr_string[caminos[s][b + 1]];
                            posi = caminos[s][b + 1];
                            colum = b + 1;
                            suma = hldr_int[caminos[s][b + 1]] + h_int[s][b + 1] + acum;
                        } else {

                        }
                    }
                }

            }
        }
        console.log("Ciudad con hldr más corto: " + actual /*+ " --> " + suma*/);
        console.log();
        acum = acum + h_int[s][colum];
    }
    if (actual=="Bucarest") {
        
        console.log("Llegamos al destino deseado: " + actual);
        break;
    }
}
// camino.push("ciudad elegida");
function buscarCiudad() {
    let pos = hldr_string.indexOf("Arad");
    return pos;
}