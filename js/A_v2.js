const hldr_string = ["Arad", "Lugoj", "Rimmicu Vilcea",
    "Craiova", "Mehadia", "Sibiu",
    "Dobreta", "Oradea", "Timisoara",
    "Fagaras", "Pitesi", "Zerind",
    "Bucarest"];

const hldr_int = [366, 244, 193,
    160, 241, 253,
    242, 380, 329,
    176, 100, 374,
    0];

const h_int = [[140, 118, 75, -1], [111, 70, -1, -1], [80, 97, 146, -1],
[120, 146, 138, -1], [70, 75, -1, -1], [140, 151, 99, 80],
[75, 120, -1, -1], [71, 151, -1, -1], [118, 111, -1, -1],
[99, 211, -1, -1], [97, 138, 101, -1], [75, 71, -1, -1]];

const caminos = [[5, 8, 11, -1], [8, 4, -1, -1], [5, 10, 3, -1],
[6, 2, 10, -1], [1, 6, -1, -1], [0, 7, 9, 2],
[4, 3, -1, -1], [11, 5, -1, -1], [0, 1, -1, -1],
[5, 12, -1, -1], [2, 3, 12, -1], [0, 7, -1, -1]];


document.write('<div id="inicio"></div>');
let ini = document.getElementById("inicio");

const selector = document.getElementById("selector");
for (let liz = 0; liz < hldr_string.length - 1; liz++) {
    selector.innerHTML += '<option value="#">' + hldr_string[liz] + '</option>';
}

ini.innerHTML +=    '<button id="empieza" onclick="a()">Empezar</button>' +
                    '<button onclick="limpiar()">Limpiar</button>'+
                    '<button id="volver"><a href="index.html">Regresar</a></button>';
    
const empieza = document.getElementById("empieza");

document.write('<div id="rutas"></div>')
let ruta = document.getElementById("rutas");

let start;
//captura lo seleccionado de la lista
function captura() {
    start = selector.options[selector.selectedIndex].text;
    empieza.style.display = 'inline-block';
}



let partida;
let actual;
let pos;
let posi;
let colum;
let menor;
let suma;
let acum;


function a() {

    posi = buscarCiudad(start);
    acum = 0;


    for (let s = 0; s < hldr_string.length; s++) {
        s = posi;
        if (posi == s) {
            ruta.innerHTML += '<br>Ciudad actual:<b> ' + hldr_string[s] + '</b><br>';
            for (let b = 0; b < 4; b++) {
                suma = hldr_int[caminos[s][b]] + h_int[s][b] + acum;

                ruta.innerHTML += '<b>Ruta ' + (b + 1) + '</b>: ' + hldr_string[caminos[s][b]] + ' - f(' + (b + 1) + ') = h(' +
                + (b + 1) +'): ' + hldr_int[caminos[s][b]] + ' + g(' + (b + 1) + '): ' + h_int[s][b] + ' + acumulado: ' + acum + ' --> f(' + (b + 1) + ') = ' + suma + '<br>';
                if (b == 3) {

                } else {
                    if (caminos[s][b + 1] == -1) {
                        break;
                    } else {
                        if (b == 0) {
                            if ((hldr_int[caminos[s][b]] + h_int[s][b] + acum) > (hldr_int[caminos[s][b + 1]] + h_int[s][b + 1]) + acum) {
                                menor = (hldr_int[caminos[s][b + 1]] + h_int[s][b + 1] + acum);
                                actual = hldr_string[caminos[s][b + 1]];
                                posi = caminos[s][b + 1];
                                colum = b + 1;
                                suma = hldr_int[caminos[s][b + 1]] + h_int[s][b + 1] + acum;
                            } else {
                                menor = (hldr_int[caminos[s][b]] + h_int[s][b] + acum);
                                actual = hldr_string[caminos[s][b]];
                                posi = caminos[s][b];
                                colum = b;
                                suma = hldr_int[caminos[s][b]] + h_int[s][b] + acum;
                            }

                        } else {
                            if (menor > (hldr_int[caminos[s][b + 1]] + h_int[s][b + 1]) + acum) {
                                menor = acum + hldr_int[caminos[s][b + 1]] + acum;
                                actual = hldr_string[caminos[s][b + 1]];
                                posi = caminos[s][b + 1];
                                colum = b + 1;
                                suma = hldr_int[caminos[s][b + 1]] + h_int[s][b + 1] + acum;
                            } else {

                            }
                        }
                    }

                }
            }

            ruta.innerHTML += 'Ciudad con f(n) más corto: <b>' + actual + '</b><br><br>';
            acum = acum + h_int[s][colum];
        }
        if (actual == "Bucarest") {
            ruta.innerHTML += 'Llegamos al destino deseado: <b>' + actual + '</b>';
            break;
        }
    }
}
//función para buscar la ciudad inicial
function buscarCiudad(inicio) {
    let pos = hldr_string.indexOf(inicio);
    return pos;
}
//limpia los recorridos
function limpiar() {
    ruta.innerHTML = '';
}